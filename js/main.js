/*Start Control Navbar Links */
let navLinks = document.querySelectorAll("header .links li a");
scrollToSection(navLinks);

/*End Control Navbar Links*/

/* Start Control Settings Menu*/

/*------Start Toggle Setting Menu*/
let toggleBtn = document.querySelector(".settings-menu .toggle-icon");
toggleBtn.onclick = function () {
  toggleBtn.children[0].classList.toggle("fa-spin");
  toggleBtn.parentElement.classList.toggle("active");
};

/*------End Toggle Setting Menu*/

/*------Start Colors Setting*/
let colorBtns = document.querySelectorAll(
  ".settings-menu .option-box .colors-list li"
);
let defaultColorBtn = document.querySelector(
  ".settings-menu .option-box .colors-list li:last-child"
);
defaultColorBtn.classList.add("active");

//Check Local Storage
if (localStorage.getItem("bio_colors_option")) {
  document.documentElement.style.setProperty(
    "--main-color",
    localStorage.getItem("bio_colors_option")
  );

  colorBtns.forEach((btn) => {
    btn.classList.remove("active");
    if (btn.dataset.color === localStorage.getItem("bio_colors_option")) {
      btn.classList.add("active");
    }
  });
}
//Click On Color Buttons
colorBtns.forEach((btn) => {
  btn.addEventListener("click", (e) => {
    colorBtns.forEach((btn) => {
      if (btn.classList.contains("active")) {
        btn.classList.remove("active");
      }
    });
    e.target.classList.add("active");
    document.documentElement.style.setProperty(
      "--main-color",
      e.target.dataset.color
    );
    localStorage.setItem("bio_colors_option", e.target.dataset.color);
  });
});
/*------End Colors Setting*/

/*------Start Random Background Control*/
let defaultBckBtn = document.querySelector(
  ".settings-menu .option-box button.yes[data-background]"
);

let randomization = true;
let sI;
let randomBacksBtns = document.querySelectorAll(
  ".settings-menu .option-box button[data-background]"
);
let landingPage = document.querySelector(".landing");
let backgrounds = ["landing1.jpg", "landing2.jpg", "landing3.jpg"];
defaultBckBtn.classList.add("active");
randomizeBacks();
//Check Local Storage

if (localStorage.getItem("bio_random_backs")) {
  randomBacksBtns.forEach((btn) => {
    btn.classList.remove("active");
    if (btn.dataset.background === localStorage.getItem("bio_random_backs")) {
      btn.classList.add("active");
      if (btn.dataset.background === "no") {
        randomization = false;
        randomizeBacks();
      } else {
        randomization = true;
        randomizeBacks();
      }
    }
  });
}
//Click On Random Backgrounds Buttons
randomBacksBtns.forEach((btn) => {
  btn.addEventListener("click", (e) => {
    randomBacksBtns.forEach((btn) => {
      if (btn.classList.contains("active")) {
        btn.classList.remove("active");
      }
    });
    e.target.classList.add("active");
    if (e.target.dataset.background === "no") {
      randomization = false;
    } else {
      randomization = true;
    }
    randomizeBacks();
    localStorage.setItem("bio_random_backs", e.target.dataset.background);
  });
});
/*------End Random Background Control*/

// /*Start Leaves Control */
let leaves = document.querySelectorAll(".leaves .leaf");
let leavesHolder = document.querySelector(".leaves");
let leavesBtns = document.querySelectorAll(
  ".settings-menu .option-box button[data-leaves]"
);
leavesBtns[0].classList.add("active");
scrollToSection(leaves);

//Check Local Storage

if (localStorage.getItem("bio_leaves_show")) {
  leavesBtns.forEach((btn) => {
    btn.classList.remove("active");
    if (btn.dataset.leaves === localStorage.getItem("bio_leaves_show")) {
      btn.classList.add("active");
      if (btn.dataset.leaves === "no") {
        leavesHolder.classList.add("hide");
      }
    }
  });
}

//Click On Leaves Show Buttons

leavesBtns.forEach((btn) => {
  btn.addEventListener("click", (e) => {
    leavesBtns.forEach((btn) => {
      if (btn.classList.contains("active")) {
        btn.classList.remove("active");
      }
    });
    e.target.classList.add("active");
    if (e.target.dataset.leaves === "no") {
      leavesHolder.classList.add("hide");
    } else {
      leavesHolder.classList.remove("hide");
    }
    localStorage.setItem("bio_leaves_show", e.target.dataset.leaves);
  });
});
/*End Leaves Control */

/*------Start Reset Contol */
let resBtn = document.querySelector(".settings-menu .option-box.reset");

resBtn.onclick = (_) => {
  if (localStorage.length) {
    localStorage.clear();
    location.reload();
  }
};
/*------End Reset Contol */

/* End Control Settings Menu*/

/*Start DropDown Menu Control */
let burgerIcon = document.querySelector("header .burger-icon");
let links = document.querySelector("header .links");

burgerIcon.addEventListener("click", (e) => {
  e.stopPropagation();
  burgerIcon.classList.toggle("active");
  links.classList.toggle("active");
});
links.onclick = (e) => {
  e.stopPropagation();
};
document.addEventListener("click", (e) => {
  if (e.target !== burgerIcon && e.target !== links) {
    if (links.classList.contains("active")) {
      burgerIcon.classList.toggle("active");
      links.classList.toggle("active");
    }
  }
});

/*End DropDown Menu Control */

/*Start Raech Contribution Section*/
let contribution = document.querySelector(".contribution");
let spans = document.querySelectorAll(
  ".contribution .content .statistic .progress span"
);
window.onscroll = () => {
  if (
    window.scrollY >=
    contribution.offsetTop //+ contribution.offsetHeight - window.innerHeight
  ) {
    spans[0].dataset.stat = "60%";
    spans[1].dataset.stat = "80%";

    spans.forEach((span) => {
      span.style.width = span.dataset.stat;
    });
  }
};
/*End Reach Contribution Section*/

/*Start Popup Control*/
let projects = document.querySelectorAll(".projects .project");

projects.forEach((project) => {
  let img = document.createElement("img");
  img.src = project.dataset.src;

  let hText = document.createTextNode(
    project.children[1].children[0].textContent
  );

  project.addEventListener("click", (e) => {
    let popup = document.createElement("div");
    popup.className = "popup";
    popup.style.opacity = "0";
    let overlay = document.createElement("div");
    overlay.className = "overlay";
    let projDesc = document.createElement("div");
    projDesc.className = "project-description";

    let h3 = document.createElement("h3");
    h3.appendChild(hText);

    let projectParagraph = document.createElement("p");
    let pText = document.createTextNode(
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem cum consequatur nisi numquam aperiam nemo expedita quisquam hic excepturi assumenda."
    );

    let closeBtn = document.createElement("span");
    closeBtn.className = "close-button";
    closeBtn.appendChild(document.createTextNode("X"));

    projectParagraph.appendChild(pText);

    projDesc.appendChild(h3);
    projDesc.appendChild(projectParagraph);

    popup.appendChild(img);
    popup.appendChild(projDesc);
    popup.appendChild(closeBtn);

    document.body.appendChild(overlay);
    window.setTimeout(() => {
      overlay.style.opacity = "1";
      popup.style.opacity = "1";

    }, 0);

    document.body.appendChild(popup);
    closeBtn.addEventListener("click", (e) => {
      e.target.parentElement.style.opacity = "0";
      overlay.style.opacity = "0";
      window.setTimeout(() => {
        e.target.parentElement.remove();
        overlay.remove();
      }, 1000);

    });
  });
});

/*End Popup Control*/

/*Start Functions */
function randomizeBacks() {
  if (randomization) {
    sI = window.setInterval(() => {
      let randomNum = Math.floor(Math.random() * backgrounds.length);
      landingPage.style.backgroundImage =
        "url(../imgs/" + backgrounds[randomNum] + ")";
    }, 5000);
  } else {
    clearInterval(sI);
  }
}

function scrollToSection(el) {
  el.forEach((link) => {
    link.addEventListener("click", (e) => {
      e.preventDefault();
      document.querySelector(e.target.dataset.section).scrollIntoView({
        behavior: "smooth",
      });
    });
  });
}
/*End Functions */
